package com.example.sr3_miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sr3MiniprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sr3MiniprojectApplication.class, args);
    }

}
