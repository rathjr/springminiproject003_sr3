package com.example.sr3_miniproject.Controller;

import com.example.sr3_miniproject.Model.FileUpload;
import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Service.RedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

@Controller
@RequestMapping("/post")
public class PostController {
    @Autowired
    RedditService redditService;
    @Autowired
    FileUpload fileUpload;

    @GetMapping("/create")
    public String createPost(Model model){
        ArrayList<Subreddit> sub= redditService.getAllSub();
        model.addAttribute("subred",sub);
        return "createpost";
    }
    @PostMapping("/create")
    public String createPost(@RequestParam("file") MultipartFile file, @ModelAttribute PostReddit postReddit){
        postReddit.setId((int)(Math.random()*10000));
        String images = fileUpload.upload(file);
        if(images.equals("")) {
            System.out.println("no image");
            System.out.println(images);
        }else{
            postReddit.setImages(images);
            System.out.println("have image");
            System.out.println(images);
        }
            ArrayList<PostReddit> post = redditService.getAllPost();
            if (redditService.insertPost(postReddit)) {
                System.out.println("insert success");
            } else System.out.println("insert fail");

        return "redirect:/";
    }

    @GetMapping("/view/{id}")
    public String getView(@PathVariable int id,Model model){
        PostReddit postReddit=redditService.findPostbyID(id);
        model.addAttribute("postid",postReddit);
        return "view";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id){
        if(redditService.deletePost(id)){
            System.out.println("remove success");
        }else
            System.out.println("remove faild");
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String getUpdate(@PathVariable int id,Model model){
        PostReddit postReddit=redditService.findPostbyID(id);
        model.addAttribute("update",postReddit);
        ArrayList<Subreddit> sub= redditService.getAllSub();
        model.addAttribute("subred",sub);
        return "update";
    }

    @PostMapping("/update")
    public String getUpdate(@RequestParam("file")MultipartFile file,@ModelAttribute PostReddit postReddit) {
        ArrayList<PostReddit> post = redditService.getAllPost();
        System.out.println(postReddit.getId());
        System.out.println("dd" +post.get(0).getId());
        for (int i = 0; i < post.size(); i++) {
            if (postReddit.getId() == post.get(i).getId()) {
                System.out.println("correct");
                    String images = fileUpload.upload(file);
                    if(images.equals("")) {
                        System.out.println("no image");;
                    }else{
                        postReddit.setImages(images);
                    }
                if (redditService.UpdatePost(postReddit)) {
                    System.out.println("update success");
                }else
                    System.out.println("update fail");
                }
            }
        return "redirect:/";

    }

}
