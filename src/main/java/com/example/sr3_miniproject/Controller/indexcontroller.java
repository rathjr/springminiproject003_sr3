package com.example.sr3_miniproject.Controller;

import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Model.Vote;
import com.example.sr3_miniproject.Service.RedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
public class indexcontroller {
    @Autowired
    RedditService redditService;

    @RequestMapping({"/", "index"})
    public String getIndex(Model model){
        ArrayList<PostReddit> post=redditService.getAllPost();
        ArrayList<Subreddit> sub= redditService.getAllSub();
        ArrayList<Vote> vote= redditService.getVote();
        model.addAttribute("pst",post);
        model.addAttribute("sub",sub);
        model.addAttribute("vt",vote);
        return "index";
    }
}
