package com.example.sr3_miniproject.Controller;

import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Service.RedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;;

import java.util.ArrayList;

@Controller
@RequestMapping("/subreddit")
public class SubredditController {

    @Autowired
    RedditService redditService;

@GetMapping("/create")
    public String getCreate(){
    return "createsubreddit";
}

    @PostMapping("/create")
    public String createSub(@ModelAttribute Subreddit subreddit){
        subreddit.setId((int)(Math.random()*10000));
        if (redditService.insertSub(subreddit)) {
            System.out.println("insert success");
        } else System.out.println("insert fail");
        return "redirect:/";
    }
    @GetMapping("/view/{id}")
    public String getView(@PathVariable int id,Model model){
        Subreddit subreddit=redditService.findSubbyID(id);
        model.addAttribute("subid",subreddit);
        return "createsubreddit";
    }
}
