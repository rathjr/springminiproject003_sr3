package com.example.sr3_miniproject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostReddit {
    private int id;
    private String title;
    private String description;
    private String sub;
    private String images;
    private String cmt;
}
