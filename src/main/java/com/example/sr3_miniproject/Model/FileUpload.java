package com.example.sr3_miniproject.Model;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileUpload {
    public String upload(MultipartFile file){
        String filename=file.getOriginalFilename();
        try{
            Path path= Paths.get("src/main/resources/files/"+filename);
            Files.copy(file.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return filename;
    }
}
