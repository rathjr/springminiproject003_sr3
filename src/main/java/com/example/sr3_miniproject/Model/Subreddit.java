package com.example.sr3_miniproject.Model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Subreddit {
    private int id;
    private String title;
    private String description;

}
