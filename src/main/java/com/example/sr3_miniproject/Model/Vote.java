package com.example.sr3_miniproject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Vote {
    private int id;
    private int value;
}
