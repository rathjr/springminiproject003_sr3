package com.example.sr3_miniproject.Repository;

import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Model.Vote;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

@Mapper
public interface RedditRepo {

    @Select("select * from subreddit")
    ArrayList<Subreddit> getAllSub();

    @Insert("insert into subreddit(id,title,description) values (#{id},#{title},#{description})")
    boolean insertSub(Subreddit subreddit);

    @Select("select * from subreddit where id=#{id}")
    Subreddit findSubbyID(int id);

    @Insert("insert into post(id,title,description,images,sub) values (#{id},#{title},#{description},#{images},#{sub})")
    boolean insertPost(PostReddit postReddit);

    @Select("select * from post")
    ArrayList<PostReddit> getAllPost();

    @Select("select * from post where id=#{id}")
    PostReddit findPostbyID(int id);

    @Delete("delete from post where id=#{id}")
    boolean deletePost(int id);

    @Select("select * from vote")
    ArrayList<Vote> getVote();

    @Update("UPDATE vote SET value = #{value} WHERE id=1 ")
    boolean insertVote(Vote vote);

    @Update("UPDATE post SET title = #{title}, description = #{description},images = #{images} WHERE id = #{id};")
    boolean UpdatePost(PostReddit postReddit);




}
