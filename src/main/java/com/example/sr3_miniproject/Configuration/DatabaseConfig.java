package com.example.sr3_miniproject.Configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Bean
    public DataSource dataSource() {

        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");

        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/reddits");

        driverManagerDataSource.setUsername("postgres");

        driverManagerDataSource.setPassword("Rathkhmer2021");

        return driverManagerDataSource;

    }
}
