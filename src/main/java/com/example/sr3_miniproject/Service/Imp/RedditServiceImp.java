package com.example.sr3_miniproject.Service.Imp;

import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Model.Vote;
import com.example.sr3_miniproject.Repository.RedditRepo;
import com.example.sr3_miniproject.Service.RedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RedditServiceImp implements RedditService {
    @Autowired
    RedditRepo redditRepo;

    @Override
    public ArrayList<Subreddit> getAllSub() {
        return redditRepo.getAllSub();
    }

    @Override
    public boolean insertSub(Subreddit subreddit) {
        return redditRepo.insertSub(subreddit);
    }

    @Override
    public boolean inSertSubreddit(Subreddit subreddit) {
        return false;
    }

    @Override
    public boolean insertPost(PostReddit postReddit) {
        return redditRepo.insertPost(postReddit);
    }

    @Override
    public ArrayList<PostReddit> getAllPost() {
        return redditRepo.getAllPost();
    }

    @Override
    public PostReddit findPostbyID(int id) {
        return redditRepo.findPostbyID(id);
    }

    @Override
    public boolean deletePost(int id) {
        return redditRepo.deletePost(id);
    }

    @Override
    public Subreddit findSubbyID(int id) {
        return redditRepo.findSubbyID(id);
    }

    @Override
    public ArrayList<Vote> getVote() {
        return redditRepo.getVote();
    }

    @Override
    public boolean insertVote(Vote vote) {
        return redditRepo.insertVote(vote);
    }

    @Override
    public boolean UpdatePost(PostReddit postReddit) {
        return redditRepo.UpdatePost(postReddit);
    }


}
