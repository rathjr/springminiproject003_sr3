package com.example.sr3_miniproject.Service;

import com.example.sr3_miniproject.Model.PostReddit;
import com.example.sr3_miniproject.Model.Subreddit;
import com.example.sr3_miniproject.Model.Vote;

import java.util.ArrayList;

public interface RedditService {
    ArrayList<Subreddit> getAllSub();
    boolean insertSub(Subreddit subreddit);
    boolean inSertSubreddit(Subreddit subreddit);
    boolean insertPost(PostReddit postReddit);
    ArrayList<PostReddit> getAllPost();
    PostReddit findPostbyID(int id);
    boolean deletePost(int id);
    Subreddit findSubbyID(int id);
    ArrayList<Vote> getVote();
    boolean insertVote(Vote vote);
    boolean UpdatePost(PostReddit postReddit);
//    boolean UpdatePost(PostReddit postReddit,int id);
}
